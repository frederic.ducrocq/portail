Bloc 2 - Algorithmique
======================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Intervenants - contacts
=======================

Benoit Papegay,
Bruno Bogaert,
Éric Wegrzynowski,
Laetitia Jourdan,
Lucien Mousin,
Marie-Émilie Voge,
Patricia Everaere,
Philippe Marquet,
Sophie Tison

Algorithmes de tri
==================

Séance 1

* [Activité d'informatique sans ordinateur - les tris](tri-sans-ordi/Readme.md)
  - [le déroulé de l'activité](tri-sans-ordi/deroule.md)
* [Reconnaître et programmer les tris](tris_anonymes/Readme.md)

Séance 2

* [Analyse en temps d'exécution des tris](analyse_tris/Readme.md)
* [Synthèse sur les tris exprimés en Français](tri-sans-ordi-correction/readme.md)

Ressources sur les tris

* Présentation et animation des tris sur le site
  [lwh.free.fr/](http://lwh.free.fr/pages/algo/tri/tri.htm)
* Des algorithmes de tri visualisés avec des danses folkloriques sur la
  [chaîne vidéo AlgoRythmics](https://www.youtube.com/user/AlgoRythmics/videos)


