Calendrier 
==========

[DIU Enseigner l'informatique au lycée](./Readme.md)


jeudi 9 mai 2019
================

C'est la rentrée ! 

| quand       | quoi                                                   | qui      | où            | avec qui                           |
|-------------|--------------------------------------------------------|----------|---------------|------------------------------------|
| 8h30        | café d'accueil                                         |          | hall du M5    |                                    |
| 9h          | amphi de rentrée - [support de présentation](doc/2019-06-diueil-slide.pdf) / [4 par page ](doc/2019-06-diueil-4up.pdf) |          | amphi Bacchus | Benoit Papegay et Philippe Marquet |
| 10h30-12h15 | [bloc 3](bloc3/Readme.md), TP Prise en main de l'environnement de travail | groupe 1 | M5-A12        | Jean-Luc, Yvan                     |
|             |                                                        | groupe 2 | M5-A13        | Laurent, Philippe                  |
|             |                                                        | groupe 3 | M5-A14        | Benoit, Jean-François              |
| 13h45-17h   | [bloc 1](bloc1/Readme.md), Travaux pratiques                              | groupe 1 | M5-A11        | Mikaël, Philippe                   |
|             |                                                        | groupe 2 | M5-A13        | Jean-Christophe, Patricia          |
|             |                                                        | groupe 3 | M5-A14        | Maude, Éric                        |

Accès au bâtiment M5 : [carte](https://osm.org/go/0B1fzL6bh--?m=) et [plan du campus](https://www.univ-lille.fr/fileadmin/user_upload/autres/Plan-site-Ulille-contact-cite%CC%81-scientifique.pdf)

* l'amphi Bacchus se situe au rdc du bâtiment M5
* les salles A11 à A16 se situent au 1er étage du M5. 


vendredi 10 mai 2019
====================

| quand       | quoi               | qui      | où     | avec qui             |
|-------------|--------------------|----------|--------|----------------------|
| 9h-10h30    | [bloc 2](bloc2/Readme.md), TD         | groupe 1 | M5-A2  | Marie-Émilie, Benoit |
|             |                    | groupe 2 | M5-A3  | Patricia, Bruno      |
|             |                    | groupe 3 | M5-A9  | Philippe, Lucien     |
| 10h45-12h15 | [bloc 2](bloc2/Readme.md), TP         | groupe 1 | M5-A16 | Marie-Émilie, Benoit |
|             |                    | groupe 2 | M5-A13 | Patricia, Bruno      |
|             |                    | groupe 3 | M5-A14 | Philippe, Lucien     |
| 13h45-14h45 | [bloc 3](bloc3/Readme.md), conférence |          | Bacchus       | Gilles, Samuel, Philippe     |
| 15h-17h     | [bloc 3](bloc3/Readme.md), TP         | groupe 1 | M5-A11 | Jean-Luc, Yvan       |
|             |                    | groupe 2 | M5-A13 | Laurent, Philippe    |
|             |                    | groupe 3 | M5-A14 | Benoit, Nicolas          |
	
mercredi 15 mai 2019
====================

matin - [bloc 1](bloc1/Readme.md)
--------------

| quand       | quoi                          | qui              | où            | avec qui               |
|-------------|-------------------------------|------------------|---------------|------------------------|
| 9h00-10h00  | Cours                         | Tous les groupes | Amphi Bacchus | Éric                   |
| 10h15-12h15 | [bloc 1](bloc1/Readme.md), TP | groupe1          | A11           | Benoit, Jean-Stéphane  |
|             |                               | groupe2          | A12           | Jean-Christophe,Fabien |
|             |                               | groupe3          | A13           | Éric, Maude            |



après-midi - [bloc 2](bloc2/Readme.md)
-------------------

(à confirmer) Marie-Émilie, Benoit, Éric, Patricia, Bruno, Jean-Stéphane

| quand       | quoi                          | qui      | où  | avec qui             |
|-------------|-------------------------------|----------|-----|----------------------|
| 13h45-15h15 | [bloc 2](bloc2/Readme.md), TD | groupe 1 | A6  | Marie-Émilie, Benoit |
|             |                               | groupe 2 | A7  | Patricia, Bruno      |
|             |                               | groupe 3 | A8  | Éric, Jean-Stéphane  |
| 15h30-17h00 | [bloc 2](bloc2/Readme.md), TP | groupe 1 | A11 | Marie-Émilie, Benoit |
|             |                               | groupe 2 | A12 | Patricia, Bruno      |
|             |                               | groupe 3 | A13 | Éric, Jean-Stéphane  |


mercredi 5 juin 2019
====================

matin - [bloc 1](bloc1/Readme.md)
--------------

| quand       | quoi                          | qui              | où            | avec qui |
|-------------|-------------------------------|------------------|---------------|----------|
| 9h00-10h00  | Cours                         | Tous les groupes | Amphi Bacchus |          |
| 10h15-12h15 | [bloc 1](bloc1/Readme.md), TP | groupe1          |               |          |
|             |                               | groupe2          |               |          |
|             |                               | groupe3          |               |          |
|             |                               |                  |               |          |

après-midi - [bloc 3](bloc3/Readme.md)
-------------------

| quand       | quoi                          | qui     | où            | avec qui |   |
|-------------|-------------------------------|---------|---------------|----------|---|
| 13h45-14h45 | Cours                         |         | Amphi Bacchus | Philippe |   |
| 15h00-17h00 | [bloc 3](bloc3/Readme.md), TP | groupe1 |               |          |   |
|             |                               | groupe2 |               |          |   |
|             |                               | groupe3 |               |          |   |

semaine du 17 juin 2019
=======================


du lundi au vendredi 

Lundi 17
--------

- Matin : Bloc 1

- Après-midi : Bloc 2
  
  | Quand       | quoi  | qui           | où       | avec qui |
  |-------------|-------|---------------|----------|----------|
  | 13h45-14h45 | Cours | Tout le monde | Amphi ?? | Sylvain  |
  |             |       |               |          |          |

Mardi 18
--------

- Matin : Bloc 1

- Après-midi : Bloc 2

  | Quand       | quoi  | qui           | où       | avec qui |
  |-------------|-------|---------------|----------|----------|
  | 13h45-14h45 | Cours | Tout le monde | Amphi ?? | Sylvain  |
  |             |       |               |          |          |

Mercredi 19
-----------

- Matin : Bloc 1

- Après-midi : Bloc 3

Jeudi 20
--------

- Matin : Bloc 1

- Après-midi : Bloc 32


Vendredi 21
-----------

- Matin : Bloc 2

- Après-midi : Conférences



semaine du 1er juillet 2019
===========================

du lundi au jeudi 

